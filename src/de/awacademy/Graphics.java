package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;


    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }


    public void draw() {
        if (model.isGameOver()) {
            drawGameOver();
            return;
        }
        if (model.isWon()) {
            drawWinningScreen();
            return;
        }

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        gc.setFill(Color.MEDIUMSEAGREEN);
        gc.fillRect(0, 0, model.WIDTH, model.HEIGHT);

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\model\\wolfrechts.png");
            Image wolfImage = new Image(inputStream);
            gc.drawImage(wolfImage, model.getWolf1().getX(), model.getWolf1().getY(), model.getWolf1().getW(), model.getWolf1().getH());
            gc.drawImage(wolfImage, model.getWolf3().getX(), model.getWolf3().getY(), model.getWolf3().getW(), model.getWolf3().getH());
            gc.drawImage(wolfImage, model.getWolf5().getX(), model.getWolf5().getY(), model.getWolf5().getW(), model.getWolf5().getH());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\model\\wolflinks.png");
            Image wolfImage = new Image(inputStream);
            gc.drawImage(wolfImage, model.getWolf2().getX(), model.getWolf2().getY(), model.getWolf2().getW(), model.getWolf2().getH());
            gc.drawImage(wolfImage, model.getWolf4().getX(), model.getWolf4().getY(), model.getWolf4().getW(), model.getWolf4().getH());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\model\\schaf.png");
            Image hundImage = new Image(inputStream);
            gc.drawImage(hundImage, model.getHund().getX(), model.getHund().getY(), model.getHund().getW(), model.getHund().getH());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        drawLittleRedRidingHood();

    }

    private void drawLittleRedRidingHood() {
        try {
            FileInputStream inputStream = new FileInputStream("src\\de\\awacademy\\model\\LittleRedRidingHood.png");
            Image lrrhImage = new Image(inputStream);
            gc.drawImage(lrrhImage, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void drawWinningScreen() {
        try {
            FileInputStream inputStream = new FileInputStream("src\\de\\awacademy\\model\\youwonmyheart.jpg");
            Image image = new Image(inputStream);
            gc.drawImage(image, 400, 100, 400, 500);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void drawGameOver() {
        try {
            FileInputStream inputStream = new FileInputStream("src\\de\\awacademy\\model\\gameover.jpg");
            Image image = new Image(inputStream);
            gc.drawImage(image, 400, 200, 300, 200);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}


//Rotkäppchen Image from: https://images.app.goo.gl/nxnYhekgfEoa8uCVA
// Image sheep:  https://images.app.goo.gl/RQnp2N4ov7Ac7iF1A
// Image you won: https://images.app.goo.gl/81MPDLFSNzxW6YX56