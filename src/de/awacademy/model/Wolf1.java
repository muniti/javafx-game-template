package de.awacademy.model;

public class Wolf1 {

    private int x;
    private int y;
    private int h;
    private int w;
    private int dx;

    public Wolf1(int x, int y, int dx) {
        this.x = x;
        this.y = y;
        this.h = 40;
        this.w = 40;
        this.dx = dx;
    }


    public void move(){
        this.x += dx;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() { return h; }

    public int getW() { return w; }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }


}
