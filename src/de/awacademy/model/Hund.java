package de.awacademy.model;

public class Hund {

    private int x;
    private int y;
    private int h;
    private int w;
    private int dx;

    public Hund(int x, int y){
        this.x = x;
        this.y = y;
        this.h = 60;
        this.w = 60;
    }

    public Hund(int x, int y, int dx){
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.h = 60;
        this.w = 60;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() { return h; }

    public int getW() { return w; }


// Schaf springt auf und ab

    public void move (){
        if (y > 595){
            this.y -= 1;
        } else {
            this.y += 4;
        }
    }

}
