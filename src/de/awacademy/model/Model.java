package de.awacademy.model;

public class Model {

    private Player player;
    private Wolf1 wolf1;
    private Wolf1 wolf2;
    private Wolf1 wolf3;
    private Wolf1 wolf4;
    private Wolf1 wolf5;
    private Hund hund;
    private boolean startgame;
    private boolean isGameOver = false;
    private boolean isWon = false;


    public static final double WIDTH = 1200;
    public static final double HEIGHT = 700;


    public Model() {
        this.player = new Player(3, 0);
        this.wolf1 = new Wolf1(10, 100, 20);
        this.wolf2 = new Wolf1(1130, 200, 25);
        this.wolf3 = new Wolf1(10, 300, 10);
        this.wolf4 = new Wolf1(1130, 400, 15);
        this.wolf5 = new Wolf1(10, 500, 25);
        this.hund = new Hund(1050, 600);
        this.startgame = false;

    }


    public Player getPlayer() {
        return player;
    }

    public void setStartgame(boolean startgame) {
        this.startgame = startgame;
    }

    public void update(long nowNano) {

        if (isWon || isGameOver) {  //ist isWon oder isGameOver true, wird die update Methode abgebrochen, d.h. Woelfe bewegen sich nicht mehr
            return;
        }

        player.move(0, 0);
        if (startgame) {
            wolf1.move();
            if (wolf1.getX() + wolf1.getDx() > WIDTH || wolf1.getX() + wolf1.getDx() < 0) {
                wolf1.setDx(-wolf1.getDx());
            }

            wolf2.move();
            if (wolf2.getX() + wolf2.getDx() > WIDTH || wolf2.getX() + wolf2.getDx() < 0) {
                wolf2.setDx(-wolf2.getDx());
            }
            wolf3.move();
            if (wolf3.getX() + wolf3.getDx() > WIDTH || wolf3.getX() + wolf3.getDx() < 0) {
                wolf3.setDx(-wolf3.getDx());
            }

            wolf4.move();
            if (wolf4.getX() + wolf4.getDx() > WIDTH || wolf4.getX() + wolf4.getDx() < 0) {
                wolf4.setDx(-wolf4.getDx());
            }

            wolf5.move();
            if (wolf5.getX() + wolf5.getDx() > WIDTH || wolf5.getX() + wolf5.getDx() < 0) {
                wolf5.setDx(-wolf5.getDx());
            }

            if (!isWon && !isGameOver){
                hund.move();
            }
        }

        if (player.getX() <= 0){
            player.setX(1);
        }
        else if (player.getX() + player.getW() >= WIDTH){
            player.setX((int)(WIDTH-(player.getW())));
        }
        else if (player.getY() <= 0){
            player.setY(1);
        }
        else if (player.getY() >= HEIGHT - player.getH()){
            player.setY((int) (HEIGHT-(player.getH())));
        }

        checkWolf1();
        checkHund();
    }

    private boolean playerIntersects(Wolf1 wolf1) {
        if(player.getX() + player.getW() > wolf1.getX() && player.getY() + player.getH() > wolf1.getY()
                && player.getX() < wolf1.getX() + wolf1.getW() && player.getY() < wolf1.getY() + wolf1.getH()){

            return true;
        }
        else{
            return false;
        }
    }

    private boolean playerIntersects(Hund hund) {
        if (player.getX() + player.getW() > hund.getX() && player.getY() + player.getH() > hund.getY()
                && player.getX() < hund.getX() + hund.getW() && player.getY() < hund.getY() + hund.getH()){
            return true;
        }

        return false;
    }

    public void checkWolf1(){

        if (playerIntersects(wolf1) || playerIntersects(wolf2) || playerIntersects(wolf3) || playerIntersects(wolf4) || playerIntersects(wolf5)){

            isGameOver = true;
        }
    }

    public void checkHund(){

        if (playerIntersects(hund)){
            isWon = true;
        }
    }
    
    public Wolf1 getWolf1() {
        return wolf1;
    }

    public Wolf1 getWolf2() {
        return wolf2;
    }

    public Wolf1 getWolf3() {
        return wolf3;
    }

    public Wolf1 getWolf4() {
        return wolf4;
    }

    public Wolf1 getWolf5() {
        return wolf5;
    }

    public Hund getHund() {
        return hund;
    }


    public boolean isGameOver() {
        return isGameOver;
    }

    public boolean isWon() {
        return isWon;
    }
}