package de.awacademy.model;

public class Player {

    private int x;
    private int y;
    private int h;
    private int w;


    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 80;
        this.w = 80;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
