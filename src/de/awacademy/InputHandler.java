package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler (Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {
        if (model.isWon() || model.isGameOver()) {
            return;
        }

        if(keycode == KeyCode.UP) {
            model.getPlayer().move(0, -10);
        }
        if(keycode == KeyCode.DOWN) {
            model.getPlayer().move(0, 10);
        }
        if(keycode == KeyCode.LEFT) {
            model.getPlayer().move(-10, 0);
        }
        if(keycode == KeyCode.RIGHT) {
            model.getPlayer().move(10, 0);
        }
        if(keycode == KeyCode.UP || keycode == KeyCode.DOWN || keycode == KeyCode.LEFT || keycode == KeyCode.RIGHT) {
            model.setStartgame(true);

        }

    }
}
